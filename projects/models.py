from django.db import models
from django.conf import settings


# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name

    def list_participants(self):
        participant_list = []
        for task in self.tasks.all():
            if str(task.assignee) not in participant_list:
                participant_list.append(str(task.assignee))
        return ", ".join(participant_list)
