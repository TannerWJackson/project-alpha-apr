from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from tasks.models import Task
from projects.forms import CreateProjectForm


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    tasks = Task.objects.filter(assignee=request.user)
    relevant_projects = []
    for project in Project.objects.all():
        for task in project.tasks.all():
            if (
                task.assignee == request.user
                and project not in relevant_projects
            ):
                relevant_projects.append(project)

    context = {
        "projects": projects,
        "tasks": tasks,
        "relevant_projects": relevant_projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


@login_required
def delete_project(request, id):
    project = Project.objects.get(id=id)
    if request.method == "POST":
        project.delete()
        return redirect("list_projects")
    context = {
        "project": project,
    }
    return render(request, "projects/delete.html", context)


@login_required
def edit_project(request, id):
    project = Project.objects.get(id=id)
    if request.method == "POST":
        form = CreateProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm(instance=project)
    context = {
        "project": project,
        "form": form,
    }
    return render(request, "projects/edit.html", context)
