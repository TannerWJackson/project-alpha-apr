from django import forms
from tasks.models import Task


class CreateTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "description",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {
            "start_date": forms.SelectDateWidget(),
            "due_date": forms.SelectDateWidget(),
        }


class EditTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "description",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "is_completed",
        ]
        widgets = {
            "start_date": forms.SelectDateWidget(),
            "due_date": forms.SelectDateWidget(),
        }
