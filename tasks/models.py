from django.db import models
from django.conf import settings
import datetime
from django.utils import timezone
from projects.models import Project


# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    description = models.TextField(null=True)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )

    def timeline_start_position(self):
        now = timezone.now()
        if self.start_date < (now - datetime.timedelta(days=5)):
            return 0
        else:
            return 5 - int((now - self.start_date).days)

    def timeline_end_position(self):
        now = timezone.now()
        if self.due_date > (now + datetime.timedelta(days=5)):
            return 10
        else:
            return int((self.due_date - now).days) + 6
