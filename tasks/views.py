from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils import timezone
import datetime
from tasks.forms import CreateTaskForm, EditTaskForm
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            start_date = form.cleaned_data["start_date"]
            due_date = form.cleaned_data["due_date"]

            if start_date <= due_date:
                form.save()
                return redirect("list_projects")

            else:
                form.add_error(
                    "due_date", "Due date must be after start date."
                )
    else:
        form = CreateTaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/my_tasks.html", context)


@login_required
def show_task_detail(request, id):
    task = get_object_or_404(Task, id=id)
    context = {
        "task": task,
    }
    return render(request, "tasks/detail.html", context)


@login_required
def edit_task(request, id):
    task = Task.objects.get(id=id)
    if request.method == "POST":
        form = EditTaskForm(request.POST, instance=task)
        if form.is_valid():
            start_date = form.cleaned_data["start_date"]
            due_date = form.cleaned_data["due_date"]
            if start_date <= due_date:
                form.save()
                return redirect("show_task_detail", id=id)

            else:
                form.add_error(
                    "due_date", "Due date must be after start date."
                )
    else:
        form = EditTaskForm(instance=task)
    context = {
        "task": task,
        "form": form,
    }
    return render(request, "tasks/edit.html", context)


@login_required
def delete_task(request, id):
    task = Task.objects.get(id=id)
    if request.method == "POST":
        task.delete()
        return redirect("list_projects")
    context = {
        "task": task,
    }
    return render(request, "tasks/delete.html", context)


@login_required
def timeline(request):
    all_tasks = Task.objects.filter(assignee=request.user)
    relevant_task_list = []
    pastfuture_task_list = []
    timeline_headers = []
    position_range = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    now = timezone.now()
    for task in all_tasks:
        if task.start_date > (
            now + datetime.timedelta(days=5)
        ) or task.due_date < (now + datetime.timedelta(days=-5)):
            pastfuture_task_list.append(task)
        else:
            relevant_task_list.append(task)
    for i in range(-5, 6):
        day = (
            str((now + datetime.timedelta(days=i)).month)
            + "/"
            + str((now + datetime.timedelta(days=i)).day)
        )
        timeline_headers.append(day)
    context = {
        "all_tasks": all_tasks,
        "relevant_task_list": relevant_task_list,
        "pastfuture_task_list": pastfuture_task_list,
        "timeline_headers": timeline_headers,
        "position_range": position_range,
    }
    return render(request, "tasks/timeline.html", context)
