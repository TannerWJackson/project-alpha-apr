from django.urls import path
from tasks.views import (
    create_task,
    show_my_tasks,
    show_task_detail,
    delete_task,
    edit_task,
    timeline,
)

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/", show_task_detail, name="show_task_detail"),
    path("<int:id>/delete/", delete_task, name="delete_task"),
    path("<int:id>/edit/", edit_task, name="edit_task"),
    path("timeline/", timeline, name="timeline"),
]
